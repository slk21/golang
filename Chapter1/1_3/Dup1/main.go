/*
	Выводит текст каждой строки, которая появляется в

стандартном вводе более одного раза, а также количество
ее появлений
*/
package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {
	counts := make(map[string]int)
	input := bufio.NewScanner(os.Stdin) // Для завершения ввода в консоль используется сочетание клавиш ctrl + D
	for input.Scan() {
		counts[input.Text()]++
	}

	for line, n := range counts {
		if n > 1 {
			fmt.Printf("\n%d\t%s\n", n, line)
		}
	}
}
