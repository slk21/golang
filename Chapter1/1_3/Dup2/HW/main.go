/*
	Выводит тест каждой строки, которая появляется во

входных данных более одного раза. Программа читает
стандартный ввод или список именованных файлов
По заданию 1.4 данная программа также выводит имена файлов в которых найдены повторяющиеся слова
*/
package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {
	var name string                   // данная переменная доавленна для содержания в себе пути файла, который потенциально может повторяться
	repeat := "Повторяющиеся файлы: " // данная переменная созданна для добавления к ней имен повторяющихся файлов
	counts := make(map[string]int)
	files := os.Args[1:]
	if len(files) == 0 {
		countLines(os.Stdin, counts, name)
	} else {
		for i, arg := range files {
			name = files[i] // в данном момене при переборе флагов (полученных имен файлов) переменной присваивается путь к файлу
			f, err := os.Open(arg)
			if err != nil {
				fmt.Fprintf(os.Stderr, "dup2: %n", err)
				continue
			}
			namerep := countLines(f, counts, name) // добаленная переменная в которую произойдет возврат из countLines
			// и добавленна переменная name для передачи в тело функции countLines
			if namerep != "" {
				// добавил условие которое проверят с каким значением вернулась переменная из countLines
				//если вернулось не пустое значит произошло было найденно поввторение и произойдет запис пути файла к переменной repeat
				repeat += "\n" + namerep
			}
			f.Close()
		}
	}
	for line, n := range counts {
		if n > 1 {
			fmt.Printf("\n%d\t%s\n", n, line)
		}
	}
	fmt.Println(repeat)
}

func countLines(f *os.File, counts map[string]int, name string) (namerep string) {
	// в список параметров добавленна переменная name для передачи в тело функци имени программы с которой идет работа
	// и доавлен именнованный возврат
	input := bufio.NewScanner(f)
	for input.Scan() {
		counts[input.Text()]++
		if counts[input.Text()] > 1 {
			// добавленно условие пр соблюдении которого имя файла передаётся переменной,
			//которая в последствии вернётся в тело функции main с именем файла
			//либо с пустым значением
			namerep = name
		}
	}
	return namerep
}
