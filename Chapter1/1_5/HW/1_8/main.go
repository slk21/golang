package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
)

func main() {
	for _, url := range os.Args[1:] {

		pref := "https://"                   //Добавленна переменная содержащая префикс, для сравнения префикса у полученной сслыки
		exam := strings.HasPrefix(url, pref) // операция сравнения префикса полученной ссылки
		if exam == false {                   // Если у ссылки отсутсвует нужный префикс, данное условие добавит его
			url = "https://" + url
		}

		resp, err := http.Get(url)
		if err != nil {
			fmt.Fprintf(os.Stderr, "fetch: %v\n", err)
			os.Exit(1)
		}
		b, err := ioutil.ReadAll(resp.Body)
		resp.Body.Close()
		if err != nil {
			fmt.Fprintf(os.Stderr, "fetch: чтение %s: %v\n", url, err)
			os.Exit(1)
		}
		fmt.Printf("%s\n", b)
	}
}
