package main

import (
	"fmt"
	"io"
	"net/http"
	"os"
)

func main() {
	for _, url := range os.Args[1:] {
		resp, err := http.Get(url)
		if err != nil {
			fmt.Fprintf(os.Stderr, "fetch: %v\n", err)
			os.Exit(1)
		}
		_, err1 := io.Copy(os.Stdout, resp.Body)
		//b, err := ioutil.ReadAll(resp.Body) данное выражение замениломь на вышестоящее и более не ребуется хранение
		//считанной инфорации, так как она сразу выводится либо обрабатывается ошибка
		resp.Body.Close()
		if err1 != nil {
			fmt.Fprintf(os.Stderr, "fetch: чтение %s: %v\n", url, err)
			os.Exit(1)
		}
	}
	fmt.Println()
}
