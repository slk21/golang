// генерирует анимированный GIF из случайных фигур Лиссажу
package main

import (
	"image"
	"image/color"
	"image/gif"
	"io"
	"math"
	"math/rand"
	"os"
	"time"
)

var palette = []color.Color{color.White, color.RGBA{24, 14, 46, 1.0}, color.RGBA{0, 128, 0, 1.0}, color.RGBA{128, 0, 0, 1.0}, color.RGBA{0, 0, 128, 1.0}, color.RGBA{47, 54, 25, 4.0}}

// Были добавленны ещё 3 цвета и убранны константы

func main() {
	lissajous(os.Stdout)
}

func lissajous(out io.Writer) {
	const (
		cycles  = 5     // Количество полных колебаний
		res     = 0.001 // Угловое разрешение
		size    = 400   // Канва изображения охватвает [size..+size]
		nframes = 5000  // Количество кадров анимации
		delay   = 1     // Задержка меду кадрами ( единица - 10мс )
	)
	rand.Seed(time.Now().UTC().UnixNano())
	freq := rand.Float64() * 3.0 // Относительная частота колебаний Y
	anim := gif.GIF{LoopCount: nframes}
	phase := 0.0 // Разность фаз
	for i := 0; i < nframes; i++ {
		rect := image.Rect(0, 0, 2*size+1, 2*size+1)
		img := image.NewPaletted(rect, palette)
		for t := 0.0; t < cycles*2*math.Pi; t += res {
			x := math.Sin(t)
			y := math.Sin(t*freq + phase)
			for i := 0; i == 0; i++ {
				r := rand.Int63n(4) + 1 // выбирает рандомное число от 1 до 5
				rq := uint8(r)          // меняет тип рандомного числа на uint8, чтобы далее использовать его в настройке цветов
				img.SetColorIndex(size+int(x*size+0.5), size+int(y*size+0.5), rq)
			}
		}
		phase += 0.1
		anim.Delay = append(anim.Delay, delay)
		anim.Image = append(anim.Image, img)
	}
	gif.EncodeAll(out, &anim)
}
